#include <iostream>
#include <pthread.h>
#include "tcp_socket.h"

using namespace std;

void* run(void* ptr)
{
	TcpSocket* tc = (TcpSocket*)ptr;
	while(true)
	{
		char buf[1024] = {};
		tc->recv(buf,sizeof(buf));
		cout << "recv:" << buf << endl;
		tc->send("return:hehe");
	}
}

int main()
{
	TcpServer* ts = new TcpServer("192.168.0.116",6789);
	while(true)
	{
		TcpSocket* tc = ts->accept();
		if(NULL == tc)
		{
			delete ts;
			return -1;
		}

		pthread_t tid = 0;
		pthread_create(&tid,NULL,run,tc);
	}
}
