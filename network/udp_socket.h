#ifndef UDP_SOCKET_H
#define UDP_SOCKET_H

#include <string.h>
#include "base_socket.h"

class UdpSocket:public BaseSocket
{
public:
	UdpSocket(const char* ip,uint16_t port):BaseSocket(SOCK_DGRAM,ip,port) {}
	int send(void* buf,size_t size)
	{
		return sendto(fd,buf,size,0,(SP)&addr,len);
	}

	int recv(void* buf,size_t size)
	{
		return recvfrom(fd,buf,size,0,(SP)&addr,&len);
	}

	int send(const char* str)
	{
		return sendto(fd,str,strlen(str)+1,0,(SP)&addr,len);
	}
};

class UdpServer:public UdpSocket
{
public:
	UdpServer(const char* ip,uint16_t port):UdpSocket(ip,port)
	{
		if(bind(fd,(SP)&addr,len))
		{
			perror("bind");
		}
	}
};

class UdpClient:public UdpSocket
{
public:
	UdpClient(const char* ip,uint16_t port):UdpSocket(ip,port)
	{
		if(connect(fd,(SP)&addr,len))
		{
			perror("connect");
		}
	}
};

#endif//UDP_SOCKET_H
