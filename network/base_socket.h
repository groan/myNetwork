#ifndef BASE_SOCKET_H
#define BASE_SOCKET_H

#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

typedef struct sockaddr* SP;

class BaseSocket
{
protected:
	int fd;
	int type;
	socklen_t len;
	struct sockaddr_in addr;
	BaseSocket(void) {}
public:
	BaseSocket(int type,const char* ip,uint16_t port)
	{
		fd = socket(AF_INET,type,0);
		if(0 > fd)
		{
			perror("socket");
			return;
		}

		len = sizeof(addr);

		addr.sin_family = AF_INET;
		addr.sin_port = htons(port);
		addr.sin_addr.s_addr = inet_addr(ip);
	}

	~BaseSocket(void)
	{
		close(fd);
	}

	virtual int send(void* buf,size_t len) = 0;

	virtual int recv(void* buf,size_t len) = 0;

};

#endif//BASE_SOCKET_H
